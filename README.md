# CAN Intro

Alcune note in menrito al bus CAN (Controller Area Network) per neofiti

## Sommario

1. Segnali nel canale fisico
1. Adattamento, Biforcazione, Baudrate, Distanze
1. bit dominante e recessivo, corrispondenza con 1 e 0; funzioni del transceiver
1. guasti simulati e qualità del transceiver
1. tempistiche: 4 parametri di configurazione del canale fisico
1. formato frame:
  - formato: CAN 11 bit, CAN 29 bit,CAN fd (idea)
  - tutti i bit del frame
1. Alcuni PinOut notevoli
1. CAN Frame per firmware-ista
1 Driver-C in generale
  - Driver C per Cangaroo
  - eventuali code HW/ Eventuali Code FW

A. partizione di indirizzi su frame
   possibile definizione dei payload
B. generiche politiche di trasmissione dati
C. Problema e proposte di risoluzione Nome ed Indirizzo
D. Trasferimento di payload maggiori di 8 byte
